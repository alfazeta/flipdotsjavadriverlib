import com.flipDot.driver.lib.FlipDots;
import com.flipDot.driver.lib.util.FlipDotsFrame;
import com.flipDot.driver.lib.util.PixelColor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Paths;

public class AnimationApp {
    public static void main(String[] args) throws InterruptedException, IOException {
        FlipDots flipDots = new FlipDots("localhost", 44000);
        String uid = Paths.get(args[0]).getFileName().toString().substring(0, 36);
        flipDots.sendAppId(uid);

        FlipDots.Resolution resolution = flipDots.getResolution();
        System.out.println("Resolution: " + resolution.width + "x" + resolution.height);

        clearScreen(flipDots, PixelColor.WHITE);

        for (int i = 0; i < 30; ++i) {
            if (i % 2 == 0) {
                drawCheckerboardAnimate(flipDots);
            }
            else {
                clearScreenAnimate(flipDots, PixelColor.WHITE);
            }
        }
    }

    static void drawText(FlipDots flipDots, String text, FlipDots.Resolution resolution) throws IOException {
        BufferedImage bufferedImage = new BufferedImage(resolution.width, resolution.height, BufferedImage.TYPE_BYTE_BINARY);
        Graphics graphics = bufferedImage.createGraphics();

        graphics.setFont(new Font("", Font.PLAIN, 8));
        graphics.drawString(text, 0, resolution.height - 1);

        FlipDotsFrame flipDotsFrame = new FlipDotsFrame(flipDots);

        for (int i = 0; i < resolution.width; ++i) {
            for (int j = 0; j < resolution.height; ++j) {
                int rgb = bufferedImage.getRGB(i, j);
                if ((rgb & 0x00ffffff) == 0) {
                    flipDotsFrame.whitePixel(i, j);
                }
                else {
                    flipDotsFrame.blackPixel(i, j);
                }
            }
        }

        flipDots.drawFrame(flipDotsFrame);
    }

    static void sendFrameAndSleep(FlipDots flipDots) throws IOException, InterruptedException {
//        byte[] frameArray = {
//        0,  1, 1, 0, 1, 1,  0,  1, 1, 1, 1,  0,  1, 1, 1, 1,  0,  1, 1, 1, 1, 1,  0,  1, 1, 1, 1,  0,
//        0,  1, 1, 1, 1, 1,  0,  1, 0, 0, 1,  0,  1, 0, 0, 1,  0,  0, 0, 1, 0, 0,  0,  1, 0, 0, 1,  0,
//        0,  1, 0, 1, 0, 1,  0,  1, 0, 0, 1,  0,  1, 0, 0, 1,  0,  0, 0, 1, 0, 0,  0,  1, 0, 0, 1,  0,
//        0,  1, 0, 1, 0, 1,  0,  1, 1, 1, 1,  0,  1, 1, 1, 1,  0,  0, 0, 1, 0, 0,  0,  1, 1, 1, 1,  0,
//        0,  1, 0, 0, 0, 1,  0,  1, 0, 0, 1,  0,  1, 1, 0, 0,  0,  0, 0, 1, 0, 0,  0,  1, 0, 0, 1,  0,
//        0,  1, 0, 0, 0, 1,  0,  1, 0, 0, 1,  0,  1, 0, 1, 0,  0,  0, 0, 1, 0, 0,  0,  1, 0, 0, 1,  0,
//        0,  1, 0, 0, 0, 1,  0,  1, 0, 0, 1,  0,  1, 0, 0, 1,  0,  0, 0, 1, 0, 0,  0,  1, 0, 0, 1,  0
//        };
        byte[] frameArray = {
                2,  1, 1, 2, 1, 1,  2,  1, 1, 1, 1,  2,  1, 1, 1, 1,  2,  1, 1, 1, 1, 1,  2,  1, 1, 1, 1,  2,
                2,  1, 1, 1, 1, 1,  2,  1, 2, 2, 1,  2,  1, 2, 2, 1,  2,  2, 2, 1, 2, 2,  2,  1, 2, 2, 1,  2,
                2,  1, 2, 1, 2, 1,  2,  1, 2, 2, 1,  2,  1, 2, 2, 1,  2,  2, 2, 1, 2, 2,  2,  1, 2, 2, 1,  2,
                2,  1, 2, 1, 2, 1,  2,  1, 1, 1, 1,  2,  1, 1, 1, 1,  2,  2, 2, 1, 2, 2,  2,  1, 1, 1, 1,  2,
                2,  1, 2, 2, 2, 1,  2,  1, 2, 2, 1,  2,  1, 1, 2, 2,  2,  2, 2, 1, 2, 2,  2,  1, 2, 2, 1,  2,
                2,  1, 2, 2, 2, 1,  2,  1, 2, 2, 1,  2,  1, 2, 1, 2,  2,  2, 2, 1, 2, 2,  2,  1, 2, 2, 1,  2,
                2,  1, 2, 2, 2, 1,  2,  1, 2, 2, 1,  2,  1, 2, 2, 1,  2,  2, 2, 1, 2, 2,  2,  1, 2, 2, 1,  2
        };

        FlipDotsFrame flipDotsFrame = new FlipDotsFrame(flipDots);
        flipDotsFrame.setFrameBuffer(frameArray);
        flipDots.drawFrame(flipDotsFrame);

        Thread.sleep(3000);
    }

    static void drawCheckerboardAnimate(FlipDots flipDots) throws IOException, InterruptedException {
        FlipDots.Resolution resolution = flipDots.getResolution();
        for (int i = 0; i < resolution.height; i++) {
            for (int j = 0; j < resolution.width; j++) {
                if ((i + j) % 2 == 0)
                    flipDots.whitePixel(j, i);
                else
                    flipDots.blackPixel(j, i);
                Thread.sleep(33);
            }
        }
    }

    static void clearScreen(FlipDots flipDots, byte pixelColor) throws IOException {
        FlipDots.Resolution resolution = flipDots.getResolution();
        for (int i = 0; i < resolution.width; i++) {
            for (int j = 0; j < resolution.height; j++) {
                flipDots.setPixel(i, j, pixelColor);
            }
        }
    }

    static void clearScreenAnimate(FlipDots flipDots, byte pixelColor) throws IOException {
        FlipDots.Resolution resolution = flipDots.getResolution();
        for (int i = 0; i < resolution.height; i++) {
            for (int j = 0; j < resolution.width; j++) {
                flipDots.setPixel(j, i, pixelColor);
                try {
                    Thread.sleep(33);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

