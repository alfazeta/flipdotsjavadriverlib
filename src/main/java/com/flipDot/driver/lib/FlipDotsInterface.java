package com.flipDot.driver.lib;

import com.flipDot.driver.lib.util.FlipDotsFrame;
import com.flipDot.driver.lib.util.managerCommunication.PeriodRefreshMetrics;

import java.io.IOException;

public interface FlipDotsInterface {

    void whitePixel(int x, int y) throws IOException;

    void blackPixel(int x, int y) throws IOException;

    void transparentPixel(int x, int y) throws IOException;;

    void setPixel(int x, int y, byte color) throws IOException;;

    public void drawFrame(FlipDotsFrame frame) throws IOException;

    public void sendAppId(String flipAppUid) throws IOException;

    public FlipDots.Resolution getResolution() throws IOException;

    public PeriodRefreshMetrics getPeriodicRefreshMetrics() throws IOException;
}
