package com.flipDot.driver.lib;

import com.flipDot.driver.lib.util.FlipDotsFrame;
import com.flipDot.driver.lib.util.OpCode;
import com.flipDot.driver.lib.util.managerCommunication.PeriodRefreshMetrics;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class FlipDots implements FlipDotsInterface {

    private DataOutputStream out;
    private DataInputStream in;
    private Socket socket;

    /**
     * Create a FlipDots object, that allows to communicate with the Maestro
     * Driver using the provided methods.
     * @param ipAddress - the ipAddress of the machine with the driver (most
     *                  of the time this will be localhost.
     * @param port - the port on which the driver listens (most likely 44000)
     * @throws IOException - when connection with the Driver fails
     */
    public FlipDots(String ipAddress, int port) throws IOException {
        socket = new Socket(ipAddress, port);
        out = new DataOutputStream(socket.getOutputStream());
        in = new DataInputStream(socket.getInputStream());
    }

    /**
     * Draw a light pixel on the display
     * @param x - x coordinate of the pixel (counting from 0, left to right)
     * @param y - y coordinate of the pixel (counting from 0, top to bottom)
     * @throws IOException - when connection with the Driver fails
     */
    public void whitePixel(int x, int y) throws IOException {
        out.writeByte(OpCode.WHITE_PIXEL);
        out.writeInt(x);
        out.writeInt(y);
        out.flush();
    }

    /**
     * Draw a dark pixel on the display
     * @param x - x coordinate of the pixel (counting from 0, left to right)
     * @param y - y coordinate of the pixel (counting from 0, top to bottom)
     * @throws IOException - when connection to Driver fails
     */
    public void blackPixel(int x, int y) throws IOException {
        out.writeByte(OpCode.BLACK_PIXEL);
        out.writeInt(x);
        out.writeInt(y);
        out.flush();
    }

    /**
     * Draw a transparent pixel on the display
     * @param x - x coordinate of the pixel (counting from 0, left to right)
     * @param y - y coordinate of the pixel (counting from 0, top to bottom)
     * @throws IOException - when connection with the Driver fails
     */
    public void transparentPixel(int x, int y) throws IOException {
        out.writeByte(OpCode.TRANSPARENT_PIXEL);
        out.writeInt(x);
        out.writeInt(y);
        out.flush();
    }

    /**
     * Draw a pixel with a given color on the display
     * @param x - x coordinate of the pixel (counting from 0, left to right)
     * @param y - y coordinate of the pixel (counting from 0, top to bottom)
     * @param color - the color code of the desired color
     * @throws IOException - when connection with the Driver fails
     */
    public void setPixel(int x, int y, byte color) throws IOException {
        out.writeByte(OpCode.SET_PIXEL);
        out.writeByte(color);
        out.writeInt(x);
        out.writeInt(y);
        out.flush();
    }

    /**
     * Draw a FlipDotsFrame on the display
     * @param frame - the FlipDotsFrame to be drawn
     * @throws IOException - when connection with the Driver fails
     */
    public void drawFrame(FlipDotsFrame frame) throws IOException {
        out.writeByte(OpCode.DRAW_FRAME);
        out.write(frame.array(), frame.arrayOffset(), frame.capacity());
    }

    /**
     * Send the FlipApp's unique identifier to the driver. This method should be called
     * before any other.
     * @param flipAppUid - the unique identifier of the FlipApp that will be send
     * @throws IOException - when connection with the Driver fails
     */
    public void sendAppId(String flipAppUid) throws IOException {
        out.writeByte(OpCode.SEND_APP_ID);
        out.writeUTF(flipAppUid);
        out.flush();
    }

    /**
     * A simple "pair" representing the width and height of the display
     */
    public static class Resolution {
        public int width;
        public int height;

        public Resolution(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    /**
     * Read the width and height of the display
     * @return a Resolution object, containing the width and height
     * @throws IOException - when connection with the Driver fails
     */
    public Resolution getResolution() throws IOException {
        out.writeByte(OpCode.GET_RESOLUTION);

        Resolution resolution = new Resolution(0, 0);
        resolution.width = in.readInt();
        resolution.height = in.readInt();

        return resolution;
    }

    public PeriodRefreshMetrics getPeriodicRefreshMetrics() throws IOException {
        out.writeByte(OpCode.GET_REFRESH_TIMESTAMP);
        long timestamp = in.readLong();
        int interval = in.readInt();
        return new PeriodRefreshMetrics(timestamp, interval);
    }
}
