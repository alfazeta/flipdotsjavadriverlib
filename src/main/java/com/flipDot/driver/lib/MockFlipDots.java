package com.flipDot.driver.lib;

import com.flipDot.driver.lib.util.FlipDotsFrame;
import com.flipDot.driver.lib.util.managerCommunication.PeriodRefreshMetrics;

import java.io.IOException;

public class MockFlipDots implements FlipDotsInterface {

    private final int width;
    private final int height;
    private boolean printFrameEnabled = false;

    public MockFlipDots(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void whitePixel(int x, int y) throws IOException {

    }

    @Override
    public void blackPixel(int x, int y) throws IOException {

    }

    @Override
    public void transparentPixel(int x, int y) throws IOException {

    }

    @Override
    public void setPixel(int x, int y, byte color) throws IOException {

    }

    @Override
    public void drawFrame(FlipDotsFrame frame) throws IOException {
        System.out.println("[" + System.currentTimeMillis() + "] drawFrame()" );
        if (printFrameEnabled) {
            printFrame(frame);
        }
    }

    public boolean isPrintFrameEnabled() {
        return printFrameEnabled;
    }

    public void setPrintFrameEnabled(boolean printFrameEnabled) {
        this.printFrameEnabled = printFrameEnabled;
    }

    public void printFrame(FlipDotsFrame frame) {
        FlipDots.Resolution resolution = frame.getResolution();
        for (int y = 0; y < resolution.height; y++) {
            for (int x = 0; x < resolution.width; x++) {
                byte color = frame.getPixel(x, y);
                if (color == 0) {
                    System.out.print(" ");
                } else {
                    System.out.print("#");
                }
            }
            System.out.println();
        }
    }

    @Override
    public void sendAppId(String flipAppUid) throws IOException {

    }

    @Override
    public FlipDots.Resolution getResolution() throws IOException {
        return new FlipDots.Resolution(width, height);
    }

    @Override
    public PeriodRefreshMetrics getPeriodicRefreshMetrics() throws IOException {
        long timestamp = System.currentTimeMillis();
        int interval = 60;
        return new PeriodRefreshMetrics(timestamp, interval);
    }
}
