package com.flipDot.driver.lib.util.managerCommunication;

public class PeriodRefreshMetrics {
    public long lastRefreshTimestamp;
    public int refreshInterval;

    public PeriodRefreshMetrics(long lastRefreshTimestamp, int refreshInterval) {
        this.lastRefreshTimestamp = lastRefreshTimestamp;
        this.refreshInterval = refreshInterval;
    }
}
