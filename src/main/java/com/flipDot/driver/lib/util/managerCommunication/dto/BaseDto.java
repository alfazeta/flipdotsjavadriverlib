package com.flipDot.driver.lib.util.managerCommunication.dto;

public abstract class BaseDto {
    protected String type;
    protected String topic;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
