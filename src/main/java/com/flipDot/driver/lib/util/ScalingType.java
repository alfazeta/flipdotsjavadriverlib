package com.flipDot.driver.lib.util;

public enum ScalingType {
    STRETCH_IMAGE,
    FIT_IMAGE,
    CUT_IMAGE
}
