package com.flipDot.driver.lib.util;

public class PixelColor {
    public final static byte WHITE          = 0x1;
    public final static byte BLACK          = 0x0;
    public final static byte TRANSPARENT    = 0x2;
}
