package com.flipDot.driver.lib.util;

public class OpCode {
    public final static byte WHITE_PIXEL        = 0x0;
    public final static byte BLACK_PIXEL        = 0x1;
    public final static byte TRANSPARENT_PIXEL  = 0x2;
    public final static byte SET_PIXEL          = 0x3;
    public final static byte DRAW_FRAME         = 0x4;
    public final static byte SEND_APP_ID        = 0x5;
    public final static byte GET_RESOLUTION     = 0x6;
    public final static byte GET_REFRESH_TIMESTAMP = 0x7;
}
