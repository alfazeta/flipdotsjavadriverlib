package com.flipDot.driver.lib.util.managerCommunication.dto;

import java.util.Map;

public class DefaultDto extends BaseDto {

    private Map<String, String> details;

    public DefaultDto() {}

    public DefaultDto(String type,
                      String topic,
                      Map <String, String> details) {
        this.type = type;
        this.topic = topic;
        this.details = details;
    }

    public Map<String, String> getDetails() {
        return details;
    }

    public void setDetails(Map<String, String> details) {
        this.details = details;
    }
}
