package com.flipDot.driver.lib.util.managerCommunication.dto;

public class IterationDto extends BaseDto {

    protected IterationDetails details;

    public IterationDto() {}

    public IterationDto(String type,
                        String topic,
                        IterationDetails details) {
        this.type = type;
        this.topic = topic;
        this.details = details;
    }

    public IterationDetails getDetails() {
        return details;
    }

    public void setDetails(IterationDetails details) {
        this.details = details;
    }

    public static class IterationDetails {
        private String message;

        public IterationDetails() {}

        public IterationDetails(String message) {
            this.message = message;
        }


        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
