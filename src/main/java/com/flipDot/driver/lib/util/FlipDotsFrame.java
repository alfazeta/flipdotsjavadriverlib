package com.flipDot.driver.lib.util;

import com.flipDot.driver.lib.FlipDots;
import com.flipDot.driver.lib.FlipDotsInterface;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

public class FlipDotsFrame {

    private byte frameColor = PixelColor.BLACK;
    private FlipDots.Resolution resolution;
    private ByteBuffer frameBuffer;

    public FlipDotsFrame(FlipDotsInterface flipDots) throws IOException {
        resolution = flipDots.getResolution();
        frameBuffer = ByteBuffer.allocate(resolution.width * resolution.height);
    }

    public FlipDotsFrame(FlipDotsInterface flipDots, byte frameColor) throws IOException {
        this.frameColor = frameColor;
        resolution = flipDots.getResolution();
        frameBuffer = ByteBuffer.allocate(resolution.width * resolution.height);
    }

    public void setPixel(int x, int y, byte color) throws RuntimeException {
        frameBuffer.put(y * resolution.width + x, color);
    }

    public byte getPixel(int x, int y) throws RuntimeException {
        return frameBuffer.get(y * resolution.width + x);
    }

    public void whitePixel(int x, int y) throws RuntimeException {
        setPixel(x, y, PixelColor.WHITE);
    }

    public void blackPixel(int x, int y) throws RuntimeException {
        setPixel(x, y, PixelColor.BLACK);
    }

    public void transparentPixel(int x, int y) throws RuntimeException {
        setPixel(x, y, PixelColor.TRANSPARENT);
    }

    public void setFrameBuffer(byte[] frame) {
        if (frame.length != frameBuffer.capacity())
            return;
        this.frameBuffer = ByteBuffer.wrap(frame);
    }

    public final int capacity() {
        return frameBuffer.capacity();
    }

    public final int arrayOffset() {
        return frameBuffer.arrayOffset();
    }

    public final byte[] array() {
        return frameBuffer.array();
    }

    public FlipDots.Resolution getResolution() {
        return resolution;
    }

    public byte getFrameColor() {
        return frameColor;
    }

    public void setFrameColor(byte frameColor) {
        this.frameColor = frameColor;
    }

    public void loadBufferedImage(BufferedImage bufferedImage, ScalingType scalingType, float threshold) {
        switch (scalingType) {
            case STRETCH_IMAGE:
                stretchImage(bufferedImage, threshold);
                break;
            case CUT_IMAGE:
                cutImage(bufferedImage, threshold);
                break;
            case FIT_IMAGE:
                fitImage(bufferedImage, threshold);
                break;
            default:
                System.out.println("Unsupported image scaling");
        }
    }

    private void stretchImage(BufferedImage bufferedImage, float threshold) {
        for (int i = 0; i < resolution.width; i++) {
            for (int j = 0; j < resolution.height; j++) {

                int x = i * bufferedImage.getWidth() / resolution.width;
                int y = j * bufferedImage.getHeight() / resolution.height;

                if (getBrightnessFromRGB(bufferedImage.getRGB(x, y)) > threshold) {
                    whitePixel(i, j);
                } else {
                    blackPixel(i, j);
                }
            }
        }
    }

    private void fitImage(BufferedImage bufferedImage, float threshold) {
        /* zero-out the buffer */
//        frameBuffer = ByteBuffer.allocate(resolution.width * resolution.height);
        clearFrame();

        int iMax, jMax;
        if (resolution.width * bufferedImage.getHeight() > resolution.height * bufferedImage.getWidth()) {
            iMax = resolution.height * bufferedImage.getWidth() / bufferedImage.getHeight();
            jMax = resolution.height;
        } else {
            iMax = resolution.width;
            jMax = resolution.width * bufferedImage.getHeight() / bufferedImage.getWidth();
        }

        int horizontalOffset, verticalOffset;
        horizontalOffset = (resolution.width - iMax) / 2;
        verticalOffset = (resolution.height - jMax) / 2;

        for (int i = 0; i < iMax; ++i) {
            for (int j = 0; j < jMax; ++j) {

                int x = i * bufferedImage.getWidth() / iMax;
                int y = j * bufferedImage.getHeight() / jMax;

                int rgba = bufferedImage.getRGB(x, y);

                if (((rgba >> 24) & 0xFF) <= 0x80) {
                    transparentPixel(i + horizontalOffset, j + verticalOffset);
                } else if (getBrightnessFromRGB(bufferedImage.getRGB(x, y)) > threshold) {
                    whitePixel(i + horizontalOffset, j + verticalOffset);
                } else {
                    blackPixel(i + horizontalOffset, j + verticalOffset);
                }
            }
        }
    }

    private void cutImage(BufferedImage bufferedImage, float threshold) {

    }

    public void clearFrame() {
        switch (frameColor) {
            case PixelColor.WHITE:
                fillFrame(PixelColor.WHITE);
                break;
            case PixelColor.TRANSPARENT:
                fillFrame(PixelColor.TRANSPARENT);
                break;
            case PixelColor.BLACK:
            default:
                fillFrame(PixelColor.BLACK);
                break;
        }
    }

    private void fillFrame(byte color) {
        for (int i = 0; i < resolution.width; ++i) {
            for (int j = 0; j < resolution.height; ++j) {
                setPixel(i, j, color);
            }
        }
    }

    private static float getBrightnessFromRGB(int color) {
        int red = (color >>> 16) & 0xFF;
        int green = (color >>> 8) & 0xFF;
        int blue = color & 0xFF;
        return (red * 0.2126f + green * 0.7152f + blue * 0.0722f) / 255;
    }
}
