package com.flipDot.driver.lib.util.managerCommunication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipDot.driver.lib.util.managerCommunication.dto.IterationDto;


public class ManagerMessagePrinter {
    private final static String commandString = "COMMAND";
    private final static String separator = " ";

    public static class Iteration {
        public static void iterationsOn() {
            printCommand(iterationsJson("ON"));
        }

        public static void iterationsOff() {
            printCommand(iterationsJson("OFF"));
        }

        public static void notifyIteration() {
            printCommand(iterationsJson("TICK"));
        }

        private static String iterationsJson(String message) {
            IterationDto iterationDto = new IterationDto(
                    MessageType.INFO.toString(),
                    MessageTopic.ITERATIONS.toString(),
                    new IterationDto.IterationDetails(message)
            );
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                return objectMapper.writeValueAsString(iterationDto);
            } catch (JsonProcessingException e) {
                return "JsonProcessingException for: " + iterationDto.toString();
            }
        }
    }

    private static void printCommand(String command) {
        System.out.println(commandString + separator + command);
    }
}
